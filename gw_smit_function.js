//gw_smit_function

// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');
var docClient = new AWS.DynamoDB.DocumentClient();

exports.handler = (event, context, callback) => {
    // TODO implement
    var cname = "SMIT"
    var ipaddress = event.state.reported.data.ip;
    var ping = event.state.reported.data.ping;
    var cputemp = event.state.reported.data.cputemp;
    var timestamp = event.state.reported.data.time;
    var dtime = event.state.reported.data.time1;
    //var dtime = event.state.reported.data.time1;
    //callback(null, event.state.reported.data.ip);
    
    var params = {
        TableName:"System_gw_health",
        Key:{
            "cname":cname
        },
        UpdateExpression: "set #ipaddress=:ipaddress, #ping=:ping, #cputemp=:cputemp, #timestamp=:timestamp, #dtime = :dtime",
        ExpressionAttributeNames:{
            "#ipaddress": "ipaddress",
            "#ping": "ping",
            "#cputemp": "cputemp",
            "#timestamp":"timestamp",
            "#dtime":"dtime"
        },
        ExpressionAttributeValues:{
            ":ipaddress":ipaddress,
            ":ping":ping,
            ":cputemp":cputemp,
            ":timestamp":timestamp,
            ":dtime":dtime
        },
        ReturnValues: "UPDATED_NEW"
    };
    
    //update lamp table
    docClient.update(params, (error,data)=>{
        if(error){
            callback(null,"ERROR: Lamp Table error: "+ error);
        }
    });
}