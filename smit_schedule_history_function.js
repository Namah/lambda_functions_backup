// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');
var docClient = new AWS.DynamoDB.DocumentClient();

exports.handler = function(e, ctx, callback){
    var tname = "";
    
    if(e.hist == "sche"){
        tname = "SMIT_schedule_history";
    }else{
        tname = "SMIT_Lamps_History";
    }
    
    var params = {
      TableName : tname  
    };
    
    docClient.scan(params, (error,data)=>{
        if(error){
            callback(null,"could not scan data");
        }else{
            callback(null,data);
        }
    });
}

