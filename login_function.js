'use strict';

var AWS = require("aws-sdk");
var dclient = new AWS.DynamoDB.DocumentClient();

exports.handler = (event, context, callback) => {
    //console.log('The event is'+event);
    var params = {
        "TableName":"User_table",
        "Key":{
            "user_name":event.uname
        }
    }
    dclient.get(params,(error,data)=>{
        if(error){
            callback(null,"User not found");
        }else{
            
             //now first check device
            if(event.device){
                
                //check device specification
                if(event.specification){
                    
                    //check token
                    if(event.token){
                        
                        //get date
                        if(event.date){
                            
                            //if user is trying to login using a mobile device
                            if(event.device == 'Mobile'){
                                
                                //first check if the token is the same, if yes then the token has been stolen
                                //also the case is when user logs out as that time both token and cookie will be both set to null
                                if(event.token == data.Item.token.mobi.value || event.token == "null"){
                
                                    //either way, this is an invalid case.
                                    callback(null,"Re-Login");
                                    
                                }else{
                                    //if tokens do not match or they are not null
                                    if(data.Item.token.mobi.validity == "null" || data.Item.token.mobi.value == "null"){
                                        //this means user is logged out
                                        //set the damn token along with specifications and validity
                                        var tname = "User_table";
                                        var colname = "token";
                                        var subcolname = "mobi";
                                        var attrname1 = "value";
                                        var attrname2 = "validity";
                                        var attrname3 = "specification";
                                        var rowid = event.uname;
                                        var token = event.token;
                                        var validity = (event.uname == "Guest_user")?(parseInt(event.date)+120000).toString():(parseInt(event.date)+900000).toString();
                                        var specification = event.specification;
                                        var lograndom = Math.floor(Math.random()* 1000000).toString();
                                        
                                        var params = {
                                            TableName: tname,
                                            Key: {
                                                "user_name": rowid
                                            },
                                            UpdateExpression: "set #column.#subcolumn.#attrname1 = :token, #column.#subcolumn.#attrname2 = :validity, #column.#subcolumn.#attrname3 = :specification, lograndom = :lograndom, loggedusing = :specification",
                                            ExpressionAttributeNames: {
                                                "#column": colname,
                                                "#subcolumn": subcolname,
                                                "#attrname1": attrname1,
                                                "#attrname2": attrname2,
                                                "#attrname3": attrname3
                                            },
                                            ExpressionAttributeValues: { 
                                                ":token": token,
                                                ":validity": validity,
                                                ":specification" : specification,
                                                ":lograndom": lograndom
                                            },
                                            ReturnValues: "UPDATED_NEW"
                                        };
                                    
                                        dclient.update(params, (error,data)=>{
                                            if(error){
                                                callback(null,"could not update token");
                                            }else{
                                                
                                                callback(null,"Re-direct%"+lograndom );
                                            }
                                        });
                                        
                                    }else{
                                        //now check the validity is valid or not
                                        var difference = parseInt(event.date) - parseInt(data.Item.token.mobi.validity);
                                        
                                        if(difference>=0){
                                            //this means token is invalid but user is not logged out yet
                                            //set token and validity
                                            var tname = "User_table";
                                            var colname = "token";
                                            var subcolname = "mobi";
                                            var attrname1 = "value";
                                            var attrname2 = "validity";
                                            var attrname3 = "specification";
                                            var rowid = event.uname;
                                            var token = event.token;
                                            var validity = (parseInt(event.date)+900000).toString();
                                            var specification = event.specification;
                                            var lograndom = Math.floor(Math.random()* 1000000).toString();
                                            
                                            var params = {
                                                TableName: tname,
                                                Key: {
                                                    "user_name": rowid
                                                },
                                                UpdateExpression: "set #column.#subcolumn.#attrname1 = :token, #column.#subcolumn.#attrname2 = :validity, #column.#subcolumn.#attrname3 = :specification, lograndom = :lograndom, loggedusing = :specification",
                                                ExpressionAttributeNames: {
                                                    "#column": colname,
                                                    "#subcolumn": subcolname,
                                                    "#attrname1": attrname1,
                                                    "#attrname2": attrname2,
                                                    "#attrname3": attrname3
                                                },
                                                ExpressionAttributeValues: { 
                                                    ":token": token,
                                                    ":validity": validity,
                                                    ":specification" : specification,
                                                    ":lograndom": lograndom
                                                },
                                                ReturnValues: "UPDATED_NEW"
                                            };
                                        
                                            dclient.update(params, (error,data)=>{
                                                if(error){
                                                    callback(null,"could not update token");
                                                }else{
                                                    
                                                    callback(null,'Re-direct%'+lograndom);
                                                }
                                            });
                                            
                                            
                                        }else{
                                            //token is valid so check if user is from the same device or a different device
                                            //callback(null, "Token still valid: You are logged in");
                                            if(event.specification == data.Item.token.mobi.specification){
                                                //same device, login token still valid
                                                //this means the user is already logged in..redirect him to index.html
                                                var params = {
                                                    "TableName":"User_table",
                                                    "Key":{
                                                        "user_name":event.uname
                                                    }
                                                }
                                                dclient.get(params, (error,data)=>{
                                                    if(error){
                                                        callback(null,"You are logged in already logged in but we are unable to find your data, please retry again");
                                                    }else{
                                                        callback(null, "Redirect%"+data.Item.lograndom);
                                                    }
                                                });
                                                
                                            }else{
                                                //different device, login token still vallid
                                                callback(null, "You can only log in with a single mobile device and PC at a time. So please log out of the other device before logging in on this one.");
                                            }
                                        }
                                    }
                                    //end of mobile case
                                }
                                
                            }else if(event.device == 'computer'){
                                
                                if(event.token == data.Item.token.comp.value || event.token == "null"){
                
                                    //either way, this is an invalid case.
                                    callback(null,"Re-Login");
                                    
                                }else{
                                    //if tokens do not match and they are not null
                                    if(data.Item.token.comp.validity == "null" || data.Item.token.comp.value == "null"){
                                        //this means user is logged out
                                        //set the damn token along with specifications and validity
                                        var tname = "User_table";
                                        var colname = "token";
                                        var subcolname = "comp";
                                        var attrname1 = "value";
                                        var attrname2 = "validity";
                                        var attrname3 = "specification";
                                        var rowid = event.uname;
                                        var token = event.token;
                                        var validity = (event.uname == "Guest_user")?(parseInt(event.date)+600000).toString():(parseInt(event.date)+900000).toString();
                                        var specification = event.specification;
                                        var lograndom = Math.floor(Math.random()* 1000000).toString();
                                        
                                        var params = {
                                            TableName: tname,
                                            Key: {
                                                "user_name": rowid
                                            },
                                            UpdateExpression: "set #column.#subcolumn.#attrname1 = :token, #column.#subcolumn.#attrname2 = :validity, #column.#subcolumn.#attrname3 = :specification, lograndom = :lograndom, loggedusing = :specification",
                                            ExpressionAttributeNames: {
                                                "#column": colname,
                                                "#subcolumn": subcolname,
                                                "#attrname1": attrname1,
                                                "#attrname2": attrname2,
                                                "#attrname3": attrname3
                                            },
                                            ExpressionAttributeValues: { 
                                                ":token": token,
                                                ":validity": validity,
                                                ":specification" : specification,
                                                ":lograndom": lograndom
                                            },
                                            ReturnValues: "UPDATED_NEW"
                                        };
                                    
                                        dclient.update(params, (error,data)=>{
                                            if(error){
                                                callback(null,"could not update token");
                                            }else{
                                                
                                                callback(null,"Re-direct%"+lograndom);
                                            }
                                        });
                                        
                                    }else{
                                        //now check the validity is valid or not
                                        var difference = parseInt(event.date) - parseInt(data.Item.token.comp.validity);
                                        
                                        if(difference>=0){
                                            //this means token is invalid but user is not logged out yet
                                            //set token and validity
                                            var tname = "User_table";
                                            var colname = "token";
                                            var subcolname = "comp";
                                            var attrname1 = "value";
                                            var attrname2 = "validity";
                                            var attrname3 = "specification";
                                            var rowid = event.uname;
                                            var token = event.token;
                                            var validity = (parseInt(event.date)+900000).toString();
                                            var specification = event.specification;
                                            var lograndom = Math.floor(Math.random()* 1000000).toString();
                                            
                                            var params = {
                                                TableName: tname,
                                                Key: {
                                                    "user_name": rowid
                                                },
                                                UpdateExpression: "set #column.#subcolumn.#attrname1 = :token, #column.#subcolumn.#attrname2 = :validity, #column.#subcolumn.#attrname3 = :specification, lograndom = :lograndom, loggedusing = :specification",
                                                ExpressionAttributeNames: {
                                                    "#column": colname,
                                                    "#subcolumn": subcolname,
                                                    "#attrname1": attrname1,
                                                    "#attrname2": attrname2,
                                                    "#attrname3": attrname3
                                                },
                                                ExpressionAttributeValues: { 
                                                    ":token": token,
                                                    ":validity": validity,
                                                    ":specification" : specification,
                                                    ":lograndom": lograndom
                                                },
                                                ReturnValues: "UPDATED_NEW"
                                            };
                                        
                                            dclient.update(params, (error,data)=>{
                                                if(error){
                                                    callback(null,"could not update token");
                                                }else{
                                                    
                                                    callback(null,'Re-direct%'+lograndom);
                                                }
                                            });
                                            
                                            
                                        }else{
                                            //token is valid so check if user is from the same device or a different device
                                            //callback(null, "Token still valid: You are logged in");
                                            if(event.specification == data.Item.token.comp.specification){
                                                //same device, login token still valid
                                                
                                                //this means the user is already logged in..redirect him to index.html
                                                var params = {
                                                    "TableName":"User_table",
                                                    "Key":{
                                                        "user_name":event.uname
                                                    }
                                                }
                                                dclient.get(params, (error,data)=>{
                                                    if(error){
                                                        callback(null,"You are logged in already logged in but we are unable to find your data, please retry again");
                                                    }else{
                                                        callback(null, "Re-direct%"+data.Item.lograndom);
                                                    }
                                                });
                                                
                                            }else{
                                                //different device, login token still vallid
                                                callback(null, "You can only log in with a single mobile device and PC at a time. So please log out of the other device before logging in on this one.");
                                            }
                                        }
                                    }
                                    //end of computer case
                                }
                            }
                            
                        }else{
                            
                            callback(null, "specify date and time");
                        }
                    
                    }else{
                        callback(null,"token not defined");    
                    }
                    
                }else{
                    callback(null,"device specifications not provided");
                }
                
            }else{
                //if device is not specified, respond with error
                callback(null,"device not specified");
            }
                
        }
    });
}