'use strict';

var AWS = require("aws-sdk");
var dclient = new AWS.DynamoDB.DocumentClient();

exports.handler = (event, context, callback) => {
    var params = {
        "TableName":"User_table",
        "Key":{
            "user_name":event.uname
        }
    };
    
    dclient.get(params,(error,data)=>{
        if(error){
            //Could not get data for the specified user
            callback(null,"Error: could not get data on client");
        }else{
            //now set validity to null
            
            //check device first
            var device = (event.device == "computer")?"comp":"mobi"; 
            var param = {
                TableName: "User_table",
                Key: {
                    "user_name": event.uname
                },
                UpdateExpression: "set #column.#subcolumn.#attrname = :value, loggedusing = :value",
                ExpressionAttributeNames: {
                    "#column": "token",
                    "#subcolumn": device,
                    "#attrname": "validity"
                },
                ExpressionAttributeValues: { 
                    ":value": "null"
                },
                ReturnValues: "UPDATED_NEW"
            };
            
            dclient.update(param, (error,data)=>{
                if(error){
                    callback(null,"could not set validity to null");
                }else{
                    callback(null,"Done");
                }
            });
        }
    });
};