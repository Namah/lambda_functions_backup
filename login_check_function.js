'use strict';

var AWS = require("aws-sdk");
var dclient = new AWS.DynamoDB.DocumentClient();

exports.handler = (event, context, callback) => {
    var params = {
        "TableName":"User_table",
        "Key":{
            "user_name":event.uname
        }
    };
    
    dclient.get(params,(error,data)=>{
        if(error){
            //Could not get data for the specified user
            callback(null,"Re-direct");
        }else{
            
            if(event.lograndom == data.Item.lograndom && event.specification == data.Item.loggedusing){
                var now = parseInt(event.date);
                
                var valid = 0;
                var token = "";
                
                if(event.device == "mobile"){
                    valid = parseInt(data.Item.token.mobi.validity);
                    token = data.Item.token.mobi.value;
                }else if(event.device == "computer"){
                    valid = parseInt(data.Item.token.comp.validity);
                    token = data.Item.token.comp.value;
                }
                
                //callback(null,(now-valid));
                
                if((now-valid)<0){
                    //this means valid is greater than now
                    
                    //token is valid
                    
                    //get post api url
                    var param = {
                        "TableName":"Customer_Table",
                        "Key":{
                            "cname":data.Item.customer_place
                        }
                    };
                    
                    dclient.get(param, (error,data)=>{
                        if(error){
                            callback(null,"Re-direct");
                        }else{
                            //callback(null,data);
                            
                            var returnval = {
                                "user_name":event.uname,
                                "validity":valid,
                                "token":token,
                                "device":event.device,
                                "post_api_url":data.Item.post_api_url,
                                "center_latitude": data.Item.center.lat,
                                "center_longitude": data.Item.center.lng,
                                "history_url":data.Item.history_url
                            }
                            callback(null,returnval);
                        }
                    });
                    
                }else{
                    //token is invalid
                    callback(null,"Re-direct");
                }
                
            }else{
                callback(null,"Re-direct");
            }
                
        }
        
    });
};