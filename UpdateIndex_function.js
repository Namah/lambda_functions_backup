'use strict';

var AWS = require("aws-sdk");
var dclient = new AWS.DynamoDB.DocumentClient();

exports.handler = (event, context, callback) => {
    var lampno = event.lamp;
    var msg = "";
    
    function concat(message){
        msg = msg + message + "%";
        //console.log(msg);
        
        if(msg.split("%").length == (lampno.length)+1){
            callback(null,msg);
        }
    }
    
    for(var i=0;i<lampno.length;i++){
        var params = {
            "TableName": "Lamp_Table",
            "Key":{
                "lampID":lampno[i]
            }
        };
        
        dclient.get(params, (error,data)=>{
            if(error){
                console.log("Could not get lamp data");
            }else{
                concat(JSON.stringify(data));
            }
        });
        
    }
    
};