'use strict';

var AWS = require("aws-sdk");
var dclient = new AWS.DynamoDB.DocumentClient();

var lamp_data = "";
var fail_lamp_data = "";

exports.handler = (event, context, callback) => {
     var params = {
        "TableName":"User_table",
        "Key":{
            "user_name":event.uname
        }
    };
    
    dclient.get(params, (error,data)=>{
        if(error){
            callback(null,"ERROR:Could not get the required data");
        }else{
            
            //now use the zone value to get the lights in that area
            
            var params = {
                "TableName":"Zone_table",
                "Key":{
                    "Zone":data.Item.role
                }
            };
            
            //NOTE: need to also send center latitude and longitude
            
            //get the list of lamps
            dclient.get(params, (error,data)=>{
                if(error){
                    callback(null,"ERROR:Could not get data on the particular zone");
                }else{
                    var lamps = data.Item.Lamps
                    callback(null,lamps);
                }
            });
        }
    });
};