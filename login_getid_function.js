'use strict';

var AWS = require("aws-sdk");
var dclient = new AWS.DynamoDB.DocumentClient();

exports.handler = (event, context, callback) => {
    //console.log('The event is'+event);
    var params = {
        "TableName":"User_table",
        "Key":{
            "user_name":event.uname
        }
    }
    
    //callback(null,event.uname);
    dclient.get(params,(error,data)=>{
        if(error){
            callback(null,"Error occured: Could not get data on the specified user.");
        }else{
            var customer = data.Item.customer_place;
            
            var params = {
                "TableName":"Customer_Table",
                "Key":{
                    "cname":customer
                }
            }
            
            dclient.get(params, (error,result)=>{
                if(error){
                    callback(null,"Could not find the specified customer");
                }else{
                    var return_str = result.Item.cliID +"%"+ result.Item.userpool;
                    callback(null,return_str);
                }
            });
        }
    });
};
