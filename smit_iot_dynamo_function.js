// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');
var docClient = new AWS.DynamoDB.DocumentClient();

exports.handler = function(e, ctx, callback){
    /**********GET COUNT FIRST*************/
    var lamp_history_count = -1;
    var schedule_history_count = -1;
    
    function setcount(val){
        //console.log(val);
        lamp_history_count = val.Item.lamp_history_count;
        schedule_history_count = val.Item.schedule_history_count;
        //console.log(lamp_history_count.toString() +"," + schedule_history_count.toString());
        return 1;
    }
    var countpar = {
        "TableName":"Customer_Table",
        "Key":{
            "cname":"SMIT"
        }
    };
    //console.log(da);
    docClient.get(countpar,(errr,dtt)=>{
        if(errr){
            console.log("Could not get count");
        }else{
            if(setcount(dtt)){
                //if count is retrieved then and only then set date
                
                /**********************SETTING DATE, TIME IN H:M:S AND MILLISECOND**************************/
                var d = new Date();
                
                //date
                var date =  d.getDate() + "-" + (parseInt(d.getMonth())+1).toString() + "-" + d.getFullYear();
                
                //time
                var h = parseInt(d.getHours()) + 5;
                var m = parseInt(d.getMinutes())+ 30;
                if(m>59){
                    m = m - 59;
                    h = h + 1;
                }
                if(h>=24){
                    var rmhr = h-24;
                    h=0;
                    h = h + rmhr;
                }
                var time = h.toString() + ":" + m.toString();
                
                //time in ms for sorting
                var timeMS = d.getTime().toString();
                
                /**********************SETTING DATE, TIME IN H:M:S AND MILLISECOND END**************************/
                
                /**********************SWITCH BETWEEN CASES****************************************************/
                if(e.state.desired){
                    
                    //this means that there is desired in the data which means it is to be posted to schedule
                    var tonhr = parseInt(e.state.desired.Time1H);
                    var tonmin = parseInt(e.state.desired.Time1M);
                    var tonint = parseInt(e.state.desired.Time1B);
                    var s1hr = parseInt(e.state.desired.Time2H);
                    var s1min = parseInt(e.state.desired.Time2M);
                    var s1int = parseInt(e.state.desired.Time2B);
                    var s2hr = parseInt(e.state.desired.Time3H);
                    var s2min = parseInt(e.state.desired.Time3M);
                    var s2int = parseInt(e.state.desired.Time3B);
                    var s3hr = parseInt(e.state.desired.Time4H);
                    var s3min = parseInt(e.state.desired.Time4M);
                    var s3int = parseInt(e.state.desired.Time4B);
                    var toffhr = parseInt(e.state.desired.Time5H);
                    var toffmin = parseInt(e.state.desired.Time5M);
                    var lampid = e.state.desired.Lamp;
                    
                    //set lamp variable
                    var lamp = "SMIT_"+lampid;
                    
                    var params = {
                        TableName: "Lamp_Table",
                        Key: {
                            "lampID": lamp
                        },
                        UpdateExpression: "set #column.#attrname1 = :tonhr, #column.#attrname2 = :tonmin, #column.#attrname3 = :tonint, #column.#attrname4 = :s1hr, #column.#attrname5 = :s1min, #column.#attrname6 = :s1int, #column.#attrname7 = :s2hr, #column.#attrname8 = :s2min, #column.#attrname9 = :s2int, #column.#attrname10 = :s3hr, #column.#attrname11 = :s3min, #column.#attrname12 = :s3int, #column.#attrname13 = :toffhr, #column.#attrname14 = :toffmin",
                        ExpressionAttributeNames: {
                            "#column": "Schedule",
                            "#attrname1": "tonhr",
                            "#attrname2": "tonmin",
                            "#attrname3": "tonint",
                            "#attrname4": "s1hr",
                            "#attrname5": "s1min",
                            "#attrname6": "s1int",
                            "#attrname7": "s2hr",
                            "#attrname8": "s2min",
                            "#attrname9": "s2int",
                            "#attrname10": "s3hr",
                            "#attrname11": "s3min",
                            "#attrname12": "s3int",
                            "#attrname13": "toffhr",
                            "#attrname14": "toffmin"
                        },
                        ExpressionAttributeValues: { 
                            ":tonhr": tonhr,
                            ":tonmin": tonmin,
                            ":tonint" : tonint,
                            ":s1hr": s1hr,
                            ":s1min": s1min,
                            ":s1int": s1int,
                            ":s2hr": s2hr,
                            ":s2min": s2min,
                            ":s2int": s2int,
                            ":s3hr": s3hr,
                            ":s3min": s3min,
                            ":s3int": s3int,
                            ":toffhr": toffhr,
                            ":toffmin": toffmin
                        },
                        ReturnValues: "UPDATED_NEW"
                    };
                    
                     var history_param = {
                        TableName: "SMIT_Schedule_History",
                        Key:{
                            "id": schedule_history_count+1
                        },
                        UpdateExpression: "set #Date = :date, #LampID = :lamp, #TimeMs = :timeMS,#Time = :time, #tonhr = :tonhr, #tonmin = :tonmin, #tonint = :tonint, #s1hr = :s1hr, #s1min = :s1min, #s1int = :s1int, #s2hr = :s2hr, #s2min = :s2min, #s2int = :s2int, #s3hr = :s3hr, #s3min = :s3min, #s3int = :s3int, #toffhr = :toffhr, #toffmin = :toffmin",
                        ExpressionAttributeNames:{
                            "#Date": "Date",
                            "#LampID": "LampID",
                            "#TimeMs":"TimeMs",
                            "#Time": "Time",
                            "#tonhr": "tonhr",
                            "#tonmin": "tonmin",
                            "#tonint" : "tonint",
                            "#s1hr": "s1hr",
                            "#s1min": "s1min",
                            "#s1int": "s1int",
                            "#s2hr": "s2hr",
                            "#s2min": "s2min",
                            "#s2int": "s2int",
                            "#s3hr": "s3hr",
                            "#s3min": "s3min",
                            "#s3int": "s3int",
                            "#toffhr": "toffhr",
                            "#toffmin": "toffmin"
                        },
                        ExpressionAttributeValues:{
                            ":date": date,
                            ":lamp": lamp,
                            ":time": time,
                            ":timeMS":timeMS,
                            ":tonhr": tonhr,
                            ":tonmin": tonmin,
                            ":tonint" : tonint,
                            ":s1hr": s1hr,
                            ":s1min": s1min,
                            ":s1int": s1int,
                            ":s2hr": s2hr,
                            ":s2min": s2min,
                            ":s2int": s2int,
                            ":s3hr": s3hr,
                            ":s3min": s3min,
                            ":s3int": s3int,
                            ":toffhr": toffhr,
                            ":toffmin": toffmin
                        },
                        ReturnValues: "UPDATED_NEW"
                    };
                    
                    /**********************UPDATE QUERY***************************************************************/
                    //update lamp table
                    docClient.update(params, (error,data)=>{
                        if(error){
                            callback(null,"ERROR: Lamp Table error: "+ error);
                        }
                    });
                    
                    //update history table
                    docClient.update(history_param, (er,dt)=>{
                        if(er){
                             callback(null,"ERROR: History Table error: "+ er);
                        }
                    });
                    
                    var update_count_reported = {
                        TableName : "Customer_Table",
                        Key:{
                            "cname": "SMIT"
                        },
                        UpdateExpression: "set #schedule_history_count = :schedule_history_count",
                        ExpressionAttributeNames:{
                            "#schedule_history_count":"schedule_history_count"
                        },
                        ExpressionAttributeValues:{
                            ":schedule_history_count": schedule_history_count + 1
                        },
                        
                        ReturnValues: "UPDATED_NEW"
                    };
                    
                    docClient.update(update_count_reported,(errrrr,dttttt)=>{
                        if(errrr){
                            console.log("Could not update count");
                        }
                    });
                    
                    /*********************UPDATE QUERY END************************************************************/ 
                    
                }else if(e.state.reported){
                     //this means that there is desired in the data which means it is to be posted to state
                     var da = e.state.reported.data;
                     
                     console.log(da.length);
                     
                     for (var i = 0; i< da.length; i++){
            
                        if(da[i].indexOf("Lamp")>=0){
                            
                            //find lamp number
                            var inx = da[i].indexOf("Lamp") + 4;
                            var num = da[i].charAt(inx);
                            var lamp = "SMIT_"+num;
                            
                            //find brightness
                            var brightness = 75;
                            
                            //find temperature
                            var intemp = da[i].indexOf("Temperature") + 13;
                            var temp = "";
                            while(da[i].charAt(intemp)!= ","){
                                if(da[i].charAt(intemp)!="}"){
                                    temp = temp + da[i].charAt(intemp).toString();
                                    intemp = intemp+1;
                                }else{
                                    break;
                                }
                            }
                            
                            //find count
                            var incount = da[i].indexOf("Count") + 7;
                            var count = "";
                            while(da[i].charAt(incount)!=","){
                                if(da[i].charAt(incount)!="}"){
                                    count = count + da[i].charAt(incount).toString();
                                    incount = incount+1;
                                }else{
                                    break;
                                }
                            }
                            
                            //find loop
                            var inloop = da[i].indexOf("Loop") + 6;
                            var loop = "";
                            while(da[i].charAt(inloop)!=","){
                                if(da[i].charAt(inloop)!="}"){
                                    loop = loop + da[i].charAt(inloop).toString();
                                    inloop = inloop+1;
                                }else{
                                    break;
                                }
                            }
                            
                            //find timenow
                            var intnow = da[i].indexOf("timenow") + 9;
                            var timenow = "";
                            while(da[i].charAt(intnow)!=","){
                                if(da[i].charAt(intnow)!="}"){
                                    timenow = timenow + da[i].charAt(intnow).toString();
                                    intnow = intnow+1;
                                }else{
                                    break;
                                }
                            }
                            
                            //find ack
                            var inack = da[i].indexOf("Ack") + 5;
                            var ack = "";
                            while(da[i].charAt(inack)!="}"){
                                if(da[i].charAt(inack)!=","){
                                    ack = ack + da[i].charAt(inack).toString();
                                    inack = inack+1;
                                }else{
                                    break;
                                }
                            }
                            
                            //find status
                            var temp_threshold = 0.0;
                            var status = (parseFloat(temp)>temp_threshold)?"Working":"Failed";
                            
                            console.log(lamp+","+temp+","+count+","+loop+","+timenow+","+ack+","+status);
                            
                            
                            var param = {
                                TableName: "Lamp_Table",
                                Key:{
                                    "lampID": lamp 
                                },
                                UpdateExpression: "set #column.#attr1 = :temp, #column.#attr2 = :count, #column.#attr3 = :loop, #column.#attr4 = :timenow, #column.#attr5 = :ack, #column.#attr6 = :status, #column.#attr7 = :brightness",
                                ExpressionAttributeNames: {
                                    "#column": "State",                    
                                    "#attr1":"Temperature",
                                    "#attr2":"Count",
                                    "#attr3":"Loop",
                                    "#attr4":"Timenow",
                                    "#attr5":"Ack",
                                    "#attr6":"Status",
                                    "#attr7":"Brightness"
                                },
                                ExpressionAttributeValues: { 
                                    ":temp": parseFloat(temp),
                                    ":count": parseInt(count),
                                    ":loop": parseInt(loop),
                                    ":timenow": parseInt(timenow),
                                    ":ack": parseInt(ack),
                                    ":status": status,
                                    ":brightness": brightness
                                },
                                ReturnValues: "UPDATED_NEW"
                            };
                            
                        //update history table
                            var history_par = {
                                TableName : "SMIT_Lamp_History",
                                Key:{
                                    "id": lamp_history_count + (i+1)
                                },
                                UpdateExpression: "set #Date = :date,#LampID = :lamp, #TimeMS = :timeMS, #Time = :time, #Temperature = :temp, #Count = :count, #Loop = :loop, #Timenow = :timenow, #Ack = :ack, #Status = :status, #Brightness = :brightness",
                                ExpressionAttributeNames:{
                                    "#Date":"Date",
                                    "#LampID":"LampID",
                                    "#TimeMS":"TimeMS",
                                    "#Time":"Time",
                                    "#Temperature":"Temperature",
                                    "#Count":"Count",
                                    "#Loop":"Loop",
                                    "#Timenow":"Timenow",
                                    "#Ack":"Ack",
                                    "#Status":"Status",
                                    "#Brightness": "Brightness"
                                },
                                ExpressionAttributeValues:{
                                    ":date":date,
                                    ":lamp":lamp,
                                    ":time":time,
                                    ":timeMS":timeMS,
                                    ":temp": parseFloat(temp),
                                    ":count": parseInt(count),
                                    ":loop": parseInt(loop),
                                    ":timenow": parseInt(timenow),
                                    ":ack": parseInt(ack),
                                    ":status": status,
                                    ":brightness": brightness
                                },
                                
                                ReturnValues: "UPDATED_NEW"
                            };
                            
                            /**********************UPDATE QUERY***************************************************************/
                            //update lamp table
                            docClient.update(param, (error,data)=>{
                                if(error){
                                    console.log("ERROR: Lamp Table error: "+ error);
                                }
                            });
                            
                            //update history table
                            docClient.update(history_par, (er,dt)=>{
                                if(er){
                                     console.log("ERROR: History Table error: "+ er);
                                }
                            });
                            
                            //update count variable
                            
                            /*********************UPDATE QUERY END************************************************************/ 

                                
                        }else{
                            callback(null,"Could not find lamp");
                        }
                        
                    }
                    //END OF FOR LOOP 
                        
                     var update_count_reported = {
                        TableName : "Customer_Table",
                        Key:{
                            "cname": "SMIT"
                        },
                        UpdateExpression: "set #lamp_history_count = :lamp_history_count",
                        ExpressionAttributeNames:{
                            "#lamp_history_count":"lamp_history_count"
                        },
                        ExpressionAttributeValues:{
                            ":lamp_history_count": lamp_history_count + da.length
                        },
                        
                        ReturnValues: "UPDATED_NEW"
                    };
                    
                    docClient.update(update_count_reported,(errrr,dtttt)=>{
                        if(errrr){
                            console.log("Could not update count");
                        }
                    });
                }
                /**********************SWITCH BETWEEN CASES END****************************************************/
                
            }
        }
    });
    
    /*****************COUNT GET END***********************************/
    
}
