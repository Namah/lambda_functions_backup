// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');
var docClient = new AWS.DynamoDB.DocumentClient();

exports.handler = function(e, ctx, callback){
    var tname = "";
    
    if(e.hist == "sche"){
        tname = "SMIT_Schedule_History";
    }else{
        tname = "SMIT_Lamp_History";
    }
    
    var upperlimit = e.upperlimit;
    var lowerlimit = e.lowerlimit;
    //console.log(tname+","+upperlimit+","+lowerlimit);
    
    var arr = [];
    function append(val){
        arr.push(val);
        console.log(arr.length);
        if(arr.length == (upperlimit - lowerlimit)){
            callback(null,arr);
        }
    }
    
    for(var i = lowerlimit; i<upperlimit; i++){
        var params = {
          "TableName" : tname,
          "Key":{
              "id": i+1
          }
        };
        
        docClient.get(params, (error,data)=>{
            if(error){
                console.log("could not scan data");
            }else{
                append(data);
            }
        });
    }
    
}