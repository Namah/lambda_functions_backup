
// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');
var docClient = new AWS.DynamoDB.DocumentClient();

exports.handler = (event, context, callback) => {
    // TODO implement
    var uname = event.uname;
    
    var get_params = {
        TableName:"User_table",
        Key:{
            "user_name":uname
        }
    };
    docClient.get(get_params, (error,data)=>{
        if(error){
            callback(null,error);
        }else{
            //callback(null,data.Item.customer_place);
            var get_gw_params = {
                TableName: "System_gw_health",
                Key:{
                    "cname":data.Item.customer_place
                }
            };
            docClient.get(get_gw_params, (er,dt)=>{
                if(er){
                    callback(null,er);
                }else{
                    callback(null,dt.Item);
                }
            });
        }
    });
    
    
}