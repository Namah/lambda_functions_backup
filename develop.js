from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
import json
from time import time, sleep


# Globals - move to env variables?
endpoint = "a1uaa04qhg9yj1.iot.ap-southeast-1.amazonaws.com"
rootCAPath = "VeriSign-Class 3-Public-Primary-Certification-Authority-G5.pem"
privateKey = "af34fefbcb-private.pem.key.key"
deviceCertificate = "af34fefbcb-certificate.pem.crt.crt"

def main(event=None, context=None):

        # Init AWSIoTMQTTClient
        myMQTTClient = AWSIoTMQTTClient("myClientID")
        myMQTTClient.configureEndpoint(endpoint, 8883)
        myMQTTClient.configureCredentials(rootCAPath, privateKey, deviceCertificate)

        # AWSIoTMQTTClient connection configuration
        #myMQTTClient.configureAutoReconnectBackoffTime(1, 32, 20)
        myMQTTClient.configureOfflinePublishQueueing(-1)  # Infinite offline Publish queueing
        myMQTTClient.configureDrainingFrequency(2)  # Draining: 2 Hz
        myMQTTClient.configureConnectDisconnectTimeout(10)  # 10 sec
        myMQTTClient.configureMQTTOperationTimeout(5)  # 5 sec

        # Connect to AWS IoT
        myMQTTClient.connect()

        # Publish
        for item in event["data"]:
                myMQTTClient.publish("$aws/things/develop/shadow/update",item, 0)
                
        #myMQTTClient.subscribe("$aws/things/develop/shadow/update/accepted", 1, customCallback)
        #myMQTTClient.unsubscribe("$aws/things/develop/shadow/update/accepted")
        myMQTTClient.disconnect()


